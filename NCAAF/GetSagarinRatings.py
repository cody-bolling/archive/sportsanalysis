import requests
from bs4 import BeautifulSoup
import csv
import sys
from EditCSV import EditCSV

page = requests.get("http://sagarin.com/sports/cfsend.htm")
soup = BeautifulSoup(page.content, "html.parser")
html = list(list(soup.children)[52].children)[1]
index = 5
sectionCount = 0
teams = []
ratings = []

for x in range (0, 256):
	team = list(list(html.children)[index])[0][6:27].strip()
	rating = list(list(html.children)[index + 1])[0][1:7].strip()

	teams.append(team)
	ratings.append(rating)

	index += 9
	sectionCount += 1

	if (sectionCount % 10 == 0):
		index += 4

CSV = EditCSV()
CSV.ReadCSV("Teams.csv")
CSV.AddColumn(sys.argv[1])

for i in range(0, len(teams)):
	CSV.AddData(teams[i], "SagarinName", ratings[i], sys.argv[1])

CSV.WriteCSV("Teams.csv")
