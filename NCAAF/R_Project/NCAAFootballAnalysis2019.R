setwd("~/Desktop/Sports_Analysis/NCAAF/R_Project")
library(readr)
library(ggplot2)

#Reading in Team and Game data
Teams = read_csv("../Teams.csv")
Games = read_csv("../Games.csv")
RawAvgs = read_csv("../TeamRawStatAvgs.csv")
AdjAvgs = read_csv("../TeamAdjStatAvgs.csv")

#Adding a new column with the score and rating differences for each game to Games
ScoreDiffs = abs(Games$HomeScore - Games$AwayScore)
Games = cbind(Games, ScoreDiffs)
CleanGames = Games[0,]

#Creating a column of the rating differences and upsets for each game and adding it to Games
RatingDiffs = vector("double")
Upset = vector("integer")
NumUpsets = 0
NumGames = 0

for (Game in 1:nrow(Games))
{
  if (Games[Game, "Subdivision"] == "FBS" | TRUE)
  {
    for (HomeTeam in 1:nrow(Teams))
    {
      if (Games[Game, "HomeTeam"] == Teams[HomeTeam, "ID"])
      {
        for (AwayTeam in 1:nrow(Teams))
        {
          if (Games[Game, "AwayTeam"] == Teams[AwayTeam, "ID"])
          {
            CurrentWeek = paste("Week", paste(8, "Rating", sep = ""), sep = "")
            
            RatingDiff = (Teams[HomeTeam, CurrentWeek] + 2.25) - Teams[AwayTeam, CurrentWeek]
            RatingDiffs = rbind(RatingDiffs, abs(RatingDiff))
            
            if ((Games[Game, "HomeScore"] > Games[Game, "AwayScore"] & RatingDiff >= 0) |
                (Games[Game, "HomeScore"] < Games[Game, "AwayScore"] & RatingDiff <= 0))
            {
              Upset = rbind(Upset, 0)
            }
            else
            {
              Upset = rbind(Upset, 1)
              NumUpsets = NumUpsets + 1
            }
            
            break
          }
        }
        
        break
      }
    }
    
    NumGames = NumGames + 1
    CleanGames = rbind(CleanGames, Games[Game,])
  }
}

colnames(RatingDiffs) = c("RatingDiffs")
CleanGames = cbind(CleanGames, RatingDiffs)
CleanGames = cbind(CleanGames, Upset)

#Cleaning up the variables no longer needed
remove("RatingDiff", "RatingDiffs", "Upset", "AwayTeam", "Game", "HomeTeam", "ScoreDiffs")

ggplot(CleanGames, aes(x = RatingDiffs, y = ScoreDiffs)) +
  geom_point(col = ifelse(CleanGames$Upset == 1, "red", "black"), pch = ifelse(CleanGames$HomeScore > CleanGames$AwayScore, "H", "A")) +
  geom_smooth(method = "auto") +
  labs(title="NCAA Football through Week 7", y="Score Difference", x="Sagarin Rating Difference") +
  scale_x_continuous(breaks=seq(0, 65, 5)) +
  scale_y_continuous(breaks=seq(0, 80, 5)) #+ coord_cartesian(xlim=c(0, 10), ylim=c(0, 30))

#Calculating Total Score statistics
TotalScore = CleanGames$HomeScore + CleanGames$AwayScore
TotalScoreAverage = mean(TotalScore)
TotalScoreMedian = median(TotalScore)

#Cleaning up the variables no longer needed
remove("TotalScore")






















