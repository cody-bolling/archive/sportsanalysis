from EditCSV import EditCSV

CSVTeams = EditCSV()
CSVTeams.ReadCSV("Teams.csv")
CSVGames = EditCSV()
CSVGames.ReadCSV("Games.csv")

for game in CSVGames.data:
    homeTeam = game[4]
    awayTeam = game[5]
    homeRating = 0
    awayRating = 0

    for team in CSVTeams.data:
        if (team[0] == homeTeam):
            homeRating = float(team[len(team) - 1]) + 2.25

        if (team[0] == awayTeam):
            awayRating = float(team[len(team) - 1])

    ratingDiff = round(abs(homeRating - awayRating), 2)

    CSVGames.AddData(game[0], 0, str(ratingDiff), "RatingDiff")

CSVGames.WriteCSV("Games.csv")
