import sys
from EditCSV import EditCSV

CSVRawStatAvg = EditCSV()
CSVRawStatAvg.ReadCSV("TeamRawStatAvgs.csv")
CSVAdjStatAvg = EditCSV()
CSVAdjStatAvg.ReadCSV("TeamAdjStatAvgs.csv")
CSVTeams = EditCSV()
CSVTeams.ReadCSV("Teams.csv")
CSVStats = EditCSV()
CSVStats.ReadCSV("GameStats.csv")

def getRawStats(teamID):
    rawStats = []

    for row in CSVStats.data:
        if (row[2] == teamID):
            statsLine = []

            for i in range(3, len(row)):
                statsLine.append(float(row[i]))

            rawStats.append(statsLine)

    return rawStats

def getAdjStats(rawStats):
    adjStats = []

    for row in rawStats:
        adjRow = []
        statMod =  1.0 - (row[len(row) - 1] / 100)

        for i in range(0, len(row) - 1):
            adjRow.append(statMod * row[i])

        adjRow.append(row[len(row) - 1])
        adjStats.append(adjRow)

    return adjStats

def getAverageRows(stats, teamID):
    averages = []
    averages.append(str(teamID))

    for i in range(0, len(stats[0])):
        sum = 0

        for j in range(0, len(stats)):
            sum = sum + stats[j][i]

        averages.append(round(sum / len(stats), 2))

    return averages

def writeData(rawAvg, adjAvg):
    CSVRawStatAvg.AddRow(rawAvg)
    CSVAdjStatAvg.AddRow(adjAvg)

CSVRawStatAvg.ClearData()
CSVAdjStatAvg.ClearData()

for team in CSVTeams.data:
    teamID = team[0]
    rawStats = getRawStats(teamID)
    adjStats = getAdjStats(rawStats)
    rawAvg = getAverageRows(rawStats, teamID)
    adjAvg = getAverageRows(adjStats, teamID)
    writeData(rawAvg, adjAvg)

CSVRawStatAvg.WriteCSV("TeamRawStatAvgs.csv")
CSVAdjStatAvg.WriteCSV("TeamAdjStatAvgs.csv")
