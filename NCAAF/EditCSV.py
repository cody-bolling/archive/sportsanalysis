import csv

class EditCSV:
    def __init__(self):
        self.colNames = []
        self.data = []

    def PrintCurrentCSV(self):
        print("\nColumn names")
        print(self.colNames)
        print("\nData")

        for d in self.data:
            print(d)

    def ReadCSV(self, csvFile):
        with open(csvFile) as csv_file:
            reader = csv.reader(csv_file, delimiter=',')
            self.colNames = next(reader)

            for row in reader:
                self.data.append(row)

    def WriteCSV(self, csvFile):
        with open(csvFile, mode='w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter = ",")
            writer.writerow(self.colNames)

            for row in self.data:
                writer.writerow(row)

    def GetValue(self, key, keyCol, valueCol):
        kCol = -1
        vCol = -1
        col = 0

        for name in self.colNames:
            if (name == keyCol):
                kCol = col
            if (name == valueCol):
                vCol = col

            col += 1

        if (kCol == -1):
            kCol = keyCol
        if (vCol == -1):
            vCol = valueCol

        for row in self.data:
            if (row[kCol].lower() == str(key).lower()):
                return row[vCol]

        return "null"

    def GetData(self):
        return self.data

    def ClearData(self):
        self.data = []

    def AddRow(self, newRow):
        self.data.append(newRow)

    def AddColumn(self, colName):
        self.colNames.append(colName)

        for row in self.data:
            row.append('')

    def AddData(self, key, keyCol, value, valueCol):
        kCol = -1
        vCol = -1
        col = 0
        success = False

        for name in self.colNames:
            if (name == keyCol):
                kCol = col
            if (name == valueCol):
                vCol = col

            col += 1

        if (kCol == -1):
            kCol = keyCol
        if (vCol == -1):
            vCol = valueCol

        for row in self.data:
            if (row[kCol] == str(key)):
                row[vCol] = str(value)
                success = True
                break

        if (success == False):
            print("\nCould not add \"" + str(value) + "\" to the row \"" + str(key) + "\"")
