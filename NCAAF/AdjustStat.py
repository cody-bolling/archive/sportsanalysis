import sys

reqStat = sys.argv[1]
t1Rating = 81.46
t2Rating = float(sys.argv[2])
stat = float(sys.argv[3])

ratingDiff = 0

if (reqStat == 1):
    ratingDiff = (t1Rating + 3.13) - t2Rating
else:
    ratingDiff = t1Rating - (t2Rating + 3.13)

statMult = (1.0 - (ratingDiff / 100))
adjStat = statMult * stat

print(adjStat)
