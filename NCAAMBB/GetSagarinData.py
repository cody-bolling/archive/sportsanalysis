from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import sys
from EditCSV import EditCSV

year = sys.argv[3]
CSV = EditCSV()
CSV.ReadCSV("Data/" + year + "/Teams.csv")
CSV.ClearData()
URL = "https://www.usatoday.com/sports/ncaab/sagarin/2019/team/"

options = webdriver.ChromeOptions()
options.add_argument("headless")
browser = webdriver.Chrome(executable_path="C:/Program Files (x86)/Google/Chrome/chromedriver.exe", options=options)
browser.get(URL)

delay = 30
try:
    myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CLASS_NAME, "sagarin-page")))
except TimeoutException:
    print("Loading took too much time!")

soup = BeautifulSoup(browser.page_source, 'html.parser')
stats = []
table = list(list(list(list(list(soup.find("div", {"class": "sagarin-page"}))[3])[1])[3])[3])
teamCount = 0

for i in range(0, 36):
    jRange = 10

    if (i == 35):
        jRange = 3

    for j in range(0, jRange):
        name = ""
        nameRow = str(table[j * 10 + 1]).split()

        for k in range(3, len(nameRow) - 1):
            name += nameRow[k].replace("&amp;", "&")

            if (k != len(nameRow) - 2):
                name += " "

        rating = str(table[j * 10 + 2]).split()[2].split('<')[0]
        recent = str(table[j * 10 + 8]).split()[2]
        conf = str(table[j * 10 + 9]).strip()

        CSV.AddRow([str(teamCount), name, "", conf, str(rating), str(recent)])
        teamCount = teamCount + 1
        #print(name)
        #print(rating)
        #print(recent)
        #print(conf)
        #print("\n")

    if (i != 35):
        table = list(list(table[9 * 10 + 12])[3])

CSV.WriteCSV("Data/" + year + "/Teams.csv")
browser.quit()
